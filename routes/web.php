<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Auth::routes(['register' => true]);
Route::middleware('auth')->group(function () {
    Route::name('admin.')->prefix('admin')->group(function () {
        // admin or editor
        Route::middleware('role:admin|editor')->group(function () {
            Route::get('/dashboard', 'admin\DashboardController@index')->name('dashboard.index');

            Route::name('profile.')->prefix('/profile-user')->group(function () {
                Route::get('/', 'admin\ProfileController@index')->name('index');
                Route::get('/edit/{id}', 'admin\ProfileController@edit')->name('edit');
                Route::post('/update{id}', 'admin\ProfileController@update')->name('update');
            });
        });

        //Role: admin
        Route::name('users.')->prefix('/users')->group(function () {
            Route::get('/', 'admin\UserController@index')->name('index')->middleware('can:list-user');
            Route::get('/create', 'admin\UserController@create')->name('create')->middleware('can:add-user');
            Route::post('/store', 'admin\UserController@store')->name('store')->middleware('can:add-user');
            Route::get('/edit/{id}', 'admin\UserController@edit')->name('edit')->middleware('can:edit-user');
            Route::post('/update/{id}', 'admin\UserController@update')->name('update')->middleware('can:edit-user');
            Route::get('/destroy/{id}', 'admin\UserController@destroy')->name('destroy')->middleware('can:delete-user');
        });

        Route::name('roles.')->prefix('/roles')->group(function () {
            Route::get('/', 'admin\RoleController@index')->name('index')->middleware('can:list-role');
            Route::get('/create', 'admin\RoleController@create')->name('create')->middleware('can:add-role');
            Route::post('/store', 'admin\RoleController@store')->name('store')->middleware('can:add-role');
            Route::get('/edit/{id}', 'admin\RoleController@edit')->name('edit')->middleware('can:edit-role');
            Route::post('/update/{id}', 'admin\RoleController@update')->name('update')->middleware('can:edit-role');
            Route::get('/destroy/{id}', 'admin\RoleController@destroy')->name('destroy')->middleware('can:delete-role');
        });

        Route::name('permissions.')->prefix('/permissions')->group(function () {
            Route::get('/', 'admin\PermissionController@index')->name('index')->middleware('can:list-permission');
            Route::post('/store', 'admin\PermissionController@store')->name('store')->middleware('can:add-permission');
            Route::get('/destroy/{id}', 'admin\PermissionController@destroy')->name('destroy')->middleware('can:delete-permission');
        });

        Route::name('chatrooms.')->prefix('/chatrooms')->group(function () {
            Route::get('/', 'admin\ChatroomController@index')->name('index')->middleware('can:list-chatroom');
            Route::get('/create', 'admin\ChatroomController@create')->name('create')->middleware('can:add-chatroom');
            Route::post('/store', 'admin\ChatroomController@store')->name('store')->middleware('can:add-chatroom');
            Route::get('/edit/{id}', 'admin\ChatroomController@edit')->name('edit')->middleware('can:edit-chatroom');
            Route::post('/update/{id}', 'admin\ChatroomController@update')->name('update')->middleware('can:edit-chatroom');
            Route::get('/destroy/{id}', 'admin\ChatroomController@destroy')->name('destroy')->middleware('can:delete-chatroom');
        });
        // Role:editor or admin
        Route::name('authors.')->prefix('/authors')->group(function () {
            Route::get('/', 'admin\AuthorController@index')->name('index')->middleware('can:list-author');
            Route::get('/create', 'admin\AuthorController@create')->name('create')->middleware('can:add-author');
            Route::post('/store', 'admin\AuthorController@store')->name('store')->middleware('can:add-author');
            Route::get('/edit/{id}', 'admin\AuthorController@edit')->name('edit')->middleware('can:edit-author');
            Route::post('/update/{id}', 'admin\AuthorController@update')->name('update')->middleware('can:edit-author');
            Route::get('/destroy/{id}', 'admin\AuthorController@destroy')->name('destroy')->middleware('can:delete-author');
        });

        Route::name('products.')->prefix('/products')->group(function () {
            Route::get('/', 'admin\ProductController@index')->name('index')->middleware('can:list-product');
            Route::get('/create', 'admin\ProductController@create')->name('create')->middleware('can:add-product');
            Route::post('/store', 'admin\ProductController@store')->name('store')->middleware('can:add-product');
            Route::get('/edit/{id}', 'admin\ProductController@edit')->name('edit')->middleware('can:edit-product');
            Route::post('/update/{id}', 'admin\ProductController@update')->name('update')->middleware('can:edit-product');
            Route::get('/show/{id}', 'admin\ProductController@show')->name('show')->middleware('can:show-product');
            Route::get('/destroy/{id}', 'admin\ProductController@destroy')->name('destroy')->middleware('can:delete-product');
        });

        Route::name('owners.')->prefix('/owners')->group(function () {
            Route::get('/', 'admin\OwnerController@index')->name('index')->middleware('can:list-owner');
            Route::get('/create', 'admin\OwnerController@create')->name('create')->middleware('can:add-owner');
            Route::post('/store', 'admin\OwnerController@store')->name('store')->middleware('can:add-owner');
            Route::get('/edit/{id}', 'admin\OwnerController@edit')->name('edit')->middleware('can:edit-owner');
            Route::post('/update/{id}', 'admin\OwnerController@update')->name('update')->middleware('can:edit-owner');
            Route::get('/destroy/{id}', 'admin\OwnerController@destroy')->name('destroy')->middleware('can:delete-owner');
        });

        Route::name('posts.')->prefix('/posts')->group(function () {
            Route::get('/', 'admin\PostController@index')->name('index')->middleware('can:list-post');
            Route::get('/create', 'admin\PostController@create')->name('create')->middleware('can:add-post');
            Route::post('/store', 'admin\PostController@store')->name('store')->middleware('can:add-post');
            Route::get('/edit/{id}', 'admin\PostController@edit')->name('edit')->middleware('can:edit-post');
            Route::post('/update/{id}', 'admin\PostController@update')->name('update')->middleware('can:edit-post');
            Route::get('/show/{id}', 'admin\PostController@show')->name('show')->middleware('can:show-post');
            Route::get('/destroy/{id}', 'admin\PostController@destroy')->name('destroy')->middleware('can:delete-post');
        });

        Route::name('faqs.')->prefix('/faqs')->group(function () {
            Route::get('/', 'admin\FaqController@index')->name('index')->middleware('can:list-faq');
            Route::get('/create', 'admin\FaqController@create')->name('create')->middleware('can:add-faq');
            Route::post('/store', 'admin\FaqController@store')->name('store')->middleware('can:add-faq');
            Route::get('/edit/{id}', 'admin\FaqController@edit')->name('edit')->middleware('can:edit-faq');
            Route::post('/update/{id}', 'admin\FaqController@update')->name('update')->middleware('can:edit-faq');
            Route::get('/destroy/{id}', 'admin\FaqController@destroy')->name('destroy')->middleware('can:delete-faq');
        });

        Route::name('tags.')->prefix('/tags')->group(function () {
            Route::get('/', 'admin\TagController@index')->name('index')->middleware('can:list-tag');
            Route::get('/create', 'admin\TagController@create')->name('create')->middleware('can:add-tag');
            Route::post('/store', 'admin\TagController@store')->name('store')->middleware('can:add-tag');
            Route::get('/edit/{id}', 'admin\TagController@edit')->name('edit')->middleware('can:edit-tag');
            Route::post('/update/{id}', 'admin\TagController@update')->name('update')->middleware('can:edit-tag');
            Route::get('/destroy/{id}', 'admin\TagController@destroy')->name('destroy')->middleware('can:delete-tag');
        });

        Route::name('categories.')->prefix('/categories')->group(function () {
            Route::get('/', 'admin\CategoryController@index')->name('index')->middleware('can:list-category');
            Route::get('/get-type', 'admin\CategoryController@getType')->name('getType')->middleware('can:list-category');
            Route::get('/create', 'admin\CategoryController@create')->name('create')->middleware('can:add-category');
            Route::post('/store', 'admin\CategoryController@store')->name('store')->middleware('can:add-category');
            Route::get('/edit/{id}', 'admin\CategoryController@edit')->name('edit')->middleware('can:edit-category');
            Route::post('/update/{id}', 'admin\CategoryController@update')->name('update')->middleware('can:edit-category');
            Route::get('/destroy/{id}', 'admin\CategoryController@destroy')->name('destroy')->middleware('can:delete-category');
        });

        Route::name('documentLaws.')->prefix('/documentLaws')->group(function () {
            Route::get('/', 'admin\DocumentLawController@index')->name('index')->middleware('can:list-documentLaw');
            Route::get('/create', 'admin\DocumentLawController@create')->name('create')->middleware('can:add-documentLaw');
            Route::post('/store', 'admin\DocumentLawController@store')->name('store')->middleware('can:add-documentLaw');
            Route::get('/edit/{id}', 'admin\DocumentLawController@edit')->name('edit')->middleware('can:edit-documentLaw');
            Route::post('/update/{id}', 'admin\DocumentLawController@update')->name('update')->middleware('can:edit-documentLaw');
            Route::get('/destroy/{id}', 'admin\DocumentLawController@destroy')->name('destroy')->middleware('can:delete-documentLaw');
        });

        Route::name('references.')->prefix('/references')->group(function () {
            Route::get('/', 'admin\ReferenceController@index')->name('index')->middleware('can:list-reference');
            Route::get('/create', 'admin\ReferenceController@create')->name('create')->middleware('can:add-reference');
            Route::post('/store', 'admin\ReferenceController@store')->name('store')->middleware('can:add-reference');
            Route::get('/edit/{id}', 'admin\ReferenceController@edit')->name('edit')->middleware('can:edit-reference');
            Route::post('/update/{id}', 'admin\ReferenceController@update')->name('update')->middleware('can:edit-reference');
            Route::get('/destroy/{id}', 'admin\ReferenceController@destroy')->name('destroy')->middleware('can:delete-reference');
        });
    });
    // User
    Route::name('posts.')->prefix('posts')->group(function () {
        Route::get('/my-post', 'home\PostController@getMyPost')->name('myPost');
        Route::post('/store', 'home\PostController@store')->name('store');
        Route::post('/update/{id}', 'home\PostController@update')->name('update');
        Route::get('/update/status/{id}', 'home\PostController@toogleStatus')->name('toogleStatus');
        Route::get('/destroy/{id}', 'home\PostController@destroy')->name('destroy');
        Route::get('/post-request', 'home\PostController@getPotRequest')->name('getPotRequest')->middleware('can:approve-post');
        Route::post('/handle-request-post/{id}', 'home\PostController@handleRequest')->name('handleRequest')->middleware('can:approve-post');
        Route::post('/connect-to-counselor/{id}', 'home\PostController@connectToCounselor')->name('connectToCounselor')->middleware('can:connect-counselor');
    });

    Route::name('comments.')->prefix('comments')->group(function () {
        Route::post('/', 'home\CommentController@store')->name('store');
        Route::post('/update/{id}', 'home\CommentController@update')->name('update');
        Route::get('/update/status/{id}', 'home\CommentController@toogleStatus')->name('toogleStatus');
        Route::get('/destroy/{id}', 'home\CommentController@destroy')->name('destroy');
    });

    Route::get('/messenger', 'home\MessengerController@index')->name('messenger.index');
    Route::get('/messenger/{id}', 'home\MessengerController@index')->name('messenger.show');
    Route::get('/messages/chatroom/{chatroom_id}', 'home\MessageController@index');
    Route::post('/messages', 'home\MessageController@store');
    Route::post('/feedback', 'home\FeedbackController@update');
    Route::get('/feedback/latest/{chatroom_id}', 'home\FeedbackController@latest');

    Route::name('profile.')->prefix('/profile-user')->group(function () {
        Route::get('/', 'home\ProfileController@index')->name('index');
        Route::get('/edit/{id}', 'home\ProfileController@edit')->name('edit');
        Route::post('/update/{id}', 'home\ProfileController@update')->name('update');
    });

    Route::get('/notifications/show-post/{id}', 'home\NotificationController@showPost')->name('notifications.showPost');
    Route::get('/notifications/mark-as-read-all', 'home\NotificationController@markAsReadAll')->name('notifications.markAsReadAll');
    Route::get('/notifications/delete-all', 'home\NotificationController@deleteAll')->name('notifications.deleteAll');
});

//No auth
Route::get('/', 'home\HomeController@index')->name('home.index');
Route::get('/faq', 'home\FaqController@index')->name('faq.index');
Route::get('/faq/category/{id}', 'home\FaqController@getByCategory')->name('faq.getByCategory');
Route::name('posts.')->prefix('posts')->group(function () {
    Route::get('/forum', 'home\PostController@index')->name('index');
    Route::get('/{id}', 'home\PostController@show')->name('show');
    Route::get('/category/{id}', 'home\PostController@getPostByCategory')->name('getPostByCategory');
    Route::get('/user/{id}', 'home\PostController@getPostByUser')->name('getPostByUser');
    Route::get('/tag/{id}', 'home\PostController@getPostByTag')->name('getPostByTag');
});

Route::name('products.')->prefix('products')->group(function () {
    Route::get('/', 'home\ProductController@index')->name('index');
    Route::get('/{id}', 'home\ProductController@show')->name('show');
    Route::get('/category/{id}', 'home\ProductController@getProductByCategory')->name('getProductByCategory');
    Route::get('/author/{id}', 'home\ProductController@getProductByAuthor')->name('getProductByAuthor');
    Route::get('/owner/{id}', 'home\ProductController@getProductByOwner')->name('getProductByOwner');
});

Route::name('documentLaws')->prefix('document-laws')->group(function () {
    Route::get('/other', 'home\DocumentLawController@index')->name('.index');
    Route::get('/person', 'home\DocumentLawController@getPersonDocument')->name('.getPersonDocument');
    Route::get('/{id}', 'home\DocumentLawController@show')->name('.show');
});
