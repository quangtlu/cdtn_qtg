<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Faq;
use Faker\Generator as Faker;

$factory->define(Faq::class, function (Faker $faker) {
    static $num = 0;
    return [
        'question' => $faker->text . ' ?',
        'answer' => $faker->text,
        'category_id' => $num++
    ];
});
