@if ($posts->count())
    @foreach ($posts as $post)
        @switch(true)
            @case(isset($isPostReference))
                @include('home.component.posts.single-post', [
                    'post' => $post,
                    'isPostReference' => $isPostReference,
                ])
            @break

            @case(isset($isPostRequest))
                @include('home.component.posts.single-post', [
                    'post' => $post,
                    'isPostRequest' => $isPostRequest,
                ])
            @break

            @case(isset($isForum))
                @include('home.component.posts.single-post', [
                    'post' => $post,
                    'isForum' => $isForum,
                ])
            @break

            @default
                @include('home.component.posts.single-post', [
                    'post' => $post,
                ])
        @endswitch
    @endforeach
    {{ $posts->withQueryString()->links() }}
@else
    <div class="alert alert-warning alert-no-post" style="margin-top: 10px" role="alert">Không có bài viết nào</div>
@endif
