@if ($documentLaws->count())
    @foreach ($documentLaws as $index => $documentLaw)
        <div class="document-wrap">
            <div class="document-left hide-on-mobile">
                <iframe type="application/pdf" width="600px" height="400px"
                    src="{{ asset('document/' . $documentLaw->url) }}"></iframe>
                <div class="document-law-footer">
                    <a href="{{ asset('document/' . $documentLaw->url) }}" target="blank"
                        class="btn button-primary button-sm">Xem ngay</a>
                    <a href="{{ asset('document/' . $documentLaw->url) }}" download
                        class="btn button-active button-sm">Tải xuống</a>
                </div>
            </div>
            <div class="document-left-info">
                <a href="{{ asset('document/' . $documentLaw->url) }}" target="blank" class="title-document">
                    <h4>{{ $documentLaw->title }}</h4>
                </a>
                <div style="padding-top: 5px">
                    {!! $documentLaw->description !!}
                </div>
            </div>
            <div class="document-law-footer only-mobile">
                <a href="{{ asset('document/' . $documentLaw->url) }}" target="blank"
                    class="btn button-primary button-sm">Xem ngay</a>
                <a href="{{ asset('document/' . $documentLaw->url) }}" download
                    class="btn button-active button-sm">Tải xuống</a>
            </div>
        </div>
    @endforeach
    {{ $documentLaws->withQueryString()->links() }}
@else
    <div class="alert alert-info alert-no-post" style="margin-top: 10px" role="alert">
        {{ isset($other) ? 'Văn bản pháp luật' : 'Tài liệu phổ cập' }} đang được cập nhật...</div>
@endif
