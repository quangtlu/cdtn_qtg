@if ($faqs->count())
    <div class="panel-group" style="margin-top: 10px" role="tablist" aria-multiselectable="true">
        @foreach ($faqs as $index => $faq)
            <div class="panel panel-custom">
                <div class="panel-heading" role="tab" id="heading{{ $faq->id }}">
                    <h4 class="panel-title">
                        <a class="collapsed" role="button" style="text-decoration: none" data-toggle="collapse"
                            href="#collapse{{ $faq->id }}" aria-expanded="false"
                            aria-controls="collapse{{ $faq->id }}">
                            {{ $faq->question }}
                        </a>
                    </h4>
                </div>
                <div id="collapse{{ $faq->id }}" class="panel-collapse collapse" role="tabpanel"
                    aria-labelledby="heading{{ $faq->id }}">
                    <div class="panel-body">
                        {!! $faq->answer !!}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endif
