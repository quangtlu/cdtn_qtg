@extends('layouts.two-column')
@section('title', 'FAQ')
@section('content')
    @include('home.component.faq.categories')
@endsection
@section('js')
    <script>
        $('#header-search-form').attr('action', '{{ route('faq.index') }}');
        $('#search-input').attr('placeholder', 'Tìm kiếm theo câu hỏi, câu trả lời...');
    </script>
@endsection
