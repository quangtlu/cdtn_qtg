<div class="" id="accordion" role="tablist" aria-multiselectable="true">
    @foreach ($refrenceCategories as $index => $category)
        <div class="panel panel-default" style="padding:10px; background-color: #fff; margin-bottom: 30px">
            <a style="font-size: 20px; font-weight:bold; display: block" class="collapsed " role="button"
                data-toggle="collapse" href="#collapse-{{ $category->id }}" aria-expanded="true"
                aria-controls="collapse-{{ $category->id }}">
                {{ $index + 1 }}.{{ $category->name }}
            </a>
            <div id="collapse-{{ $category->id }}" class="panel-collapse in collapse" role="tabpanel"
                aria-labelledby="headingTwo">
                @include('home.component.faq.list', ['faqs' => $category->faqs])
                @if ($category->categories->count())
                    @foreach ($category->categories as $categoryChild)
                        @include('home.component.faq.list', [
                            'faqs' => $categoryChild->faqs,
                        ])
                    @endforeach
                @endif
            </div>
        </div>
    @endforeach
    @if ($categories->count())
        @foreach ($categories as $index => $category)
            <div class="panel panel-default" style="padding:10px; background-color: #fff;">
                <a style="font-size: 20px; font-weight:bold" class="collapsed " role="button" data-toggle="collapse"
                    href="#collapse-{{ $category->id }}" aria-expanded="true"
                    aria-controls="collapse-{{ $category->id }}">
                    {{ $refrenceCategories->count() + $index + 1 }}. {{ $category->name }}
                </a>
                <div id="collapse-{{ $category->id }}" class="panel-collapse in collapse" role="tabpanel"
                    aria-labelledby="headingTwo">
                    @include('home.component.faq.list', ['faqs' => $category->faqs])
                    @if ($category->categories->count())
                        @foreach ($category->categories as $categoryChild)
                            @include('home.component.faq.list', [
                                'faqs' => $categoryChild->faqs,
                            ])
                        @endforeach
                    @endif
                </div>
            </div>
        @endforeach
    @endif
    @if ($faqs->count())
        <div class="panel panel-default" style="padding:10px; background-color: #fff;">
            <a style="font-size: 20px; font-weight:bold" class="collapsed " role="button" data-toggle="collapse"
                href="#faq-other" aria-expanded="true" aria-controls="faq-other">Chủ đề khác
            </a>
            <div id="faq-other" class="panel-collapse in collapse" role="tabpanel" aria-labelledby="headingTwo">
                @include('home.component.faq.list', [
                    'faqs' => $faqs,
                ])
            </div>
        </div>
    @endif
</div>
