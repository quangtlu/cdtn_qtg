@extends('layouts.two-column')
@section('title', 'Tài liệu phổ cập')
@section('content')
    @include('home.component.document-laws.list', ['documentLaws' => $documentLaws, 'other' => false])
@endsection
@section('js')
    <script>
        $('#header-search-form').attr('action', '');
        $('#search-input').attr('placeholder', 'Tìm kiếm tài liệu phổ cập...');
    </script>
@endsection
