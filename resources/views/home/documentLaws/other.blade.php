@extends('layouts.two-column')
@section('title', 'Văn bản pháp luật')
@section('content')
    @include('home.component.document-laws.list', ['documentLaws' => $documentLaws, 'other' => true])
@endsection
@section('js')
    <script>
        $('#header-search-form').attr('action', '');
        $('#search-input').attr('placeholder', 'Tìm kiếm văn bản pháp luật...');
    </script>
@endsection
