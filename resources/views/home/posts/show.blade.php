@extends('layouts.two-column')
@section('title', $post->title)
@section('css')
    <link href="{{ asset('AdminLTE/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('home/post/style.css') }}">
@endsection
@section('content')
    @include('home.component.posts.single-post', ['post' => $post])
    @if ($postRelates->count())
        <div style="margin-top: 30px">
            <h3 class="title-relate">Bài viết liên quan</h3>
            @include('home.component.posts.list-post', ['posts' => $postRelates])
        </div>
    @endif
@endsection
@section('js')
    <script src="{{ asset('AdminLTE/plugins/select2/js/select2.full.min.js') }}"></script>
    <script>
        $('[data-toggle="tooltip"]').tooltip();
        // select2
        $(".select2_init").select2();
    </script>
@endsection
