@extends('layouts.main')
@section('title', $post->title ?? 'Quyền sở hữu trí tuệ')
@section('css')
    <link rel="stylesheet" href="{{ asset('home/OwlCarousel2-2.3.4/dist/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('home/OwlCarousel2-2.3.4/dist/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('home/post/style.css') }}">
    <link rel="stylesheet" href="{{ asset('home/post/show.css') }}">
    <style>
        .w3l_categories .side-bar-heading,
        .w3l_categories .side-bar-footer,
        .post-container {
            border-radius: 0;
        }
    </style>
@endsection
@section('container-fluid')
    <div style="margin-top: 5rem; display: flex; justify-content:space-between">
        <i class="fa fa-bars only-mobile list-category-icon"></i>
        <div class="col-md-3 side-bar-left">@include('partials.home.list-category')</div>
        <div class="{{ $post && $post->references->count() > 0 ? 'col-md-6' : 'col-md-9' }} col-xs-12">
            @if ($post)
                @include('home.component.posts.single-post', ['post' => $post, 'isPostReference' => true])
            @else
                <div class="alert alert-info alert-no-post" style="margin-top: 10px" role="alert">Bài viết đang được cập
                    nhật...
                </div>
            @endif
            {{-- <div class="home-section" style="margin-top: 15px">
                <h4 class="title-section-page">Top chuyên gia tư vấn</h4>
                <div class="owl-carousel owl-theme owl-loaded"
                    style="background-image: url(https://thanglong.edu.vn/sites/default/files/2020-05/facilities-top-01.jpg);
                background-size:cover; background-repeat:repeat">
                    <div class="owl-stage-outer">
                        <div class="owl-stage">
                            @foreach ($counselors as $index => $counselor)
                                <div class="owl-item">
                                    <div class="slide-item-post-wrap">
                                        <img class="slide-item-post-authour-avt"
                                            src="{{ asset('image/profile') . '/' . $counselor->image }}" alt="">
                                        <span class="slide-item-post-author-name limit-line-2">{{ $counselor->name }}</span>
                                        <div class="row">
                                            <a href="" class="btn button-active">Hẹn tư
                                                vấn</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div> --}}

        </div>
        @if ($post && $post->references->count() > 0)
            <div class="col-md-3 side-bar-right hide-on-mobile">@include('partials.home.list-referecnce', ['references' => $post->references])</div>
        @endif
    </div>
@endsection
{{-- @section('js')
    <script src="{{ asset('home/OwlCarousel2-2.3.4/dist/owl.carousel.min.js') }}"></script>
    <script>
        $('.owl-carousel').owlCarousel({
            loop: true,
            responsiveClass: true,
            autoplay: true,
            autoplayTimeout: 5000,
            margin: 20,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 3,
                    nav: true
                },
                1000: {
                    items: 3,
                    nav: true,
                    loop: true
                }
            }
        })
    </script>
    <style>
        .owl-carousel .owl-item img {
            height: auto;
            width: 60%;
        }
    </style>
@endsection --}}
