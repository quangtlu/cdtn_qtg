@extends('layouts.two-column')
@section('title', 'FAQ')
@section('content')
    <div class="home-section" style="margin-bottom: 3rem">
        <p class="heading-faq">Nếu bạn có câu hỏi, hãy <a href="{{ route('posts.index') }}">click vào đây</a> để đi đến <a
                href="{{ route('posts.index') }}">diễn đàn hỏi đáp</a> của chúng tôi để đặt câu hỏi và trao đổi trực tiếp,
            nơi có
            những thành viên có thể cũng đang quan tâm đến vấn đề của bạn</p>
    </div>
    @include('home.component.faq.categories', ['categories' => $categories, 'faqs' => $faqs])
@endsection
@section('js')
    <script>
        $('#header-search-form').attr('action', '{{ route('faq.index') }}');
        $('#search-input').attr('placeholder', 'Tìm kiếm theo câu hỏi, câu trả lời...');
    </script>
@endsection
