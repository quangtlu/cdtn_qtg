@extends('layouts.two-column')
@section('title', 'FAQ')
@section('content')
    @if ($searchResults->count())
        @include('home.component.faq.list', ['faqs' => $searchResults])
        {{ $searchResults->withQueryString()->links() }}
    @else
        <div class="alert alert-info" style="margin-top: 10px" role="alert">FAQ đang được cập nhật...</div>
    @endif
@endsection
@section('js')
    <script>
        $('#header-search-form').attr('action', '{{ route('faq.index') }}');
        $('#search-input').attr('placeholder', 'Tìm kiếm theo câu hỏi, câu trả lời...');
    </script>
@endsection
