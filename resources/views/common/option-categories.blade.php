@if (isset($categoryId))
    @foreach ($categories as $index => $category)
        @if ($category->type == config('consts.category.type.post_reference.value') && $category->parent_id == 0)
            <option {{ $categoryId == $category->id ? 'selected' : '' }} value="{{ $category->id }}">
                {{ $index + 1 . '. ' . $category->name }}
            </option>
            @if ($category->categories->count())
                @foreach ($category->categories as $indexChild => $categoryChild)
                    <option {{ $categoryId == $categoryChild->id ? 'selected' : '' }} value="{{ $categoryChild->id }}">--
                        {{ $index + 1 . '.' . ($indexChild + 1) . '. ' . $categoryChild->name }}
                    </option>
                @endforeach
            @endif
        @elseif ($category->type != config('consts.category.type.post_reference.value'))
            <option {{ $categoryId == $category->id ? 'selected' : '' }} value="{{ $category->id }}">
                {{ $category->name }}
            </option>
        @endif
    @endforeach
@elseif(isset($modelHasCategories))
    @foreach ($categories as $index => $category)
        @if ($category->type == config('consts.category.type.post_reference.value') && $category->parent_id == 0)
            <option {{ $modelHasCategories->contains('id', $category->id) ? 'selected' : '' }}
                value="{{ $category->id }}">
                {{ $index + 1 . '. ' . $category->name }}
            </option>
            @if ($category->categories->count())
                @foreach ($category->categories as $indexChild => $categoryChild)
                    <option {{ $modelHasCategories->contains('id', $categoryChild->id) ? 'selected' : '' }}
                        value="{{ $categoryChild->id }}">
                        --
                        {{ $index + 1 . '.' . ($indexChild + 1) . '. ' . $categoryChild->name }}
                    </option>
                @endforeach
            @endif
        @elseif ($category->type != config('consts.category.type.post_reference.value'))
            <option {{ $modelHasCategories->contains('id', $category->id) ? 'selected' : '' }}
                value="{{ $category->id }}">
                {{ $category->name }}
            </option>
        @endif
    @endforeach
@else
    @foreach ($categories as $index => $category)
        @if ($category->type == config('consts.category.type.post_reference.value') && $category->parent_id == 0)
            <option value="{{ $category->id }}">
                {{ $index + 1 . '. ' . $category->name }}</option>
            @if ($category->categories->count())
                @foreach ($category->categories as $indexChild => $categoryChild)
                    <option value="{{ $categoryChild->id }}">
                        --{{ $index + 1 . '.' . ($indexChild + 1) . '. ' . $categoryChild->name }}
                    </option>
                @endforeach
            @endif
        @elseif ($category->type != config('consts.category.type.post_reference.value'))
            <option value="{{ $category->id }}">
                {{ $category->name }}</option>
        @endif
    @endforeach
@endif
