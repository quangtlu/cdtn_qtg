@extends('layouts.admin')
@section('title', 'Sửa văn bản pháp luật')
@section('css')
    <link rel="stylesheet" href="{{ asset('admin/user/create.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/product/index.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="container pt-2">
                <form action="{{ route('admin.documentLaws.update', ['id' => $documentLaw->id]) }}" method="POST"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="label-required" for="category_name">Tên văn bản pháp luật</label>
                        <input type="text" value="{{ old('title') ?? $documentLaw->title }}" name="title"
                            class="form-control">
                        @error('title')
                            <span class="mt-1 text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="label-required" for="category_name">Tệp đính kèm</label>
                        <input type="file" accept="file/*" class="form-control-file" name="url" cols="30"
                            rows="5" value="{{ $url }}">
                        <a href="{{ asset("document/$url") }}"></a>
                        @error('url')
                            <span class="mt-1 text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Thumbnail</label>
                        <input type="file" accept="image/*" class="form-control-file" name="thumbnail" cols="30"
                            rows="5" value="{{ $thumbnail }}">
                        @error('thumbnail')
                            <span class="mt-1 text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="type" class="label-required">Loại tài liệu</label>
                        <select name="type" id="type" class="form-control">
                            <option value=""></option>
                            @foreach (config('consts.documentLaw.type') as $type)
                                <option {{ $documentLaw->type == $type['value'] ? 'selected' : '' }}
                                    value="{{ $type['value'] }}">{{ $type['name'] }}</option>
                            @endforeach
                        </select>
                        @error('type')
                            <span class="mt-1 text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="category_name">Mô tả</label>
                        <textarea class="editor form-control" value="{{ old('description') ?? $documentLaw->description }}" name="description"
                            cols="30" rows="5">{{ $documentLaw->description }}</textarea>
                        @error('description')
                            <span class="mt-1 text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Cập nhật</button>
                </form>
            </div>
        </div>
        <!-- /.content -->
    </div>
@endsection
