@extends('layouts.admin')
@section('title', 'Quản lý văn bản pháp luật')
@section('css')
    <link rel="stylesheet" href="{{ asset('admin/product/index.css') }}" />
    <style>
        .document-thumb-img {
            height: 32px;
            width: 32px;
        }
    </style>
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="card w-100 mt-2">
                        <div class="card-body">
                            <div>
                                <a class="btn btn-success btn-sm" href="{{ route('admin.documentLaws.create') }}">Thêm
                                    mới</a>
                            </div>
                            <form class="pt-2" action="{{ route('admin.documentLaws.index') }}" method="get">
                                <div class="row">
                                    <div class="col-md-2">
                                        <select name="type" value="{{ request()->type }}" id="type"
                                            class="form-control">
                                            <option value="">Loại tài liệu</option>
                                            @foreach (config('consts.documentLaw.type') as $type)
                                                <option {{ request()->type == $type['value'] ? 'selected' : '' }}
                                                    value="{{ $type['value'] }}">
                                                    {{ $type['name'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="text" name="keyword" placeholder="Tìm kiếm tên văn bản pháp luật"
                                            class="form-control" value="{{ request()->keyword }}">
                                    </div>
                                    <div class="col-md-1">
                                        <button type="submit" class="btn btn-info btn-block"><i
                                                class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12 card">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Ảnh</th>
                                    <th>Tiêu đề</th>
                                    <th>Loại tài liệu</th>
                                    <th>Tệp đính kèm</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($documentLaws as $documentLaw)
                                    <tr>
                                        <td>{{ $documentLaw->id }}</td>
                                        <td>
                                            @if ($documentLaw->thumbnail)
                                                <img class="document-thumb-img"
                                                    src="{{ asset('image/documentLaws') . '/' . $documentLaw->thumbnail }}">
                                            @else
                                                <img class="document-thumb-img"
                                                    src="{{ asset('image/documentLaws/default.png') }}">
                                            @endif
                                        </td>
                                        <td class="limit-line-2">{{ $documentLaw->title }}</td>
                                        <td>
                                            @foreach (config('consts.documentLaw.type') as $type)
                                                @if ($type['value'] == $documentLaw->type)
                                                    {{ $type['name'] }}
                                                @endif
                                            @endforeach
                                        </td>
                                        <td><a href="{{ asset('document/' . $documentLaw->url) }}"
                                                target="_blank">{{ $documentLaw->url }}</a></td>
                                        <td>
                                            <a href="{{ route('admin.documentLaws.edit', ['id' => $documentLaw->id]) }}"><button
                                                    class="btn btn-info btn-sm">Sửa</button></a>
                                            <button type="button"
                                                data-url="{{ route('admin.documentLaws.destroy', ['id' => $documentLaw->id]) }}"
                                                class="btn btn-danger btn-sm btn-delete">Xóa</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $documentLaws->withQueryString()->links() }}
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection
@section('js')
    <script>
        $('#header-search-form').attr('action', '{{ route('admin.documentLaws.index') }}');
        $('#search-input').attr('placeholder', 'Tìm kiếm tên văn bản pháp luật');
    </script>
@endsection
