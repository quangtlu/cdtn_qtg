@extends('layouts.admin')
@section('title', 'Thêm mới văn bản pháp luật')
@section('css')
    <link rel="stylesheet" href="{{ asset('admin/user/create.css') }}">
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="content">
            <div class="container pt-2">
                <form action="{{ route('admin.documentLaws.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label class="label-required" for="category_name">Tên văn bản pháp luật</label>
                        <input type="text" name="title" class="form-control" value="{{ old('title') }}">
                        @error('title')
                            <span class="mt-1 text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="label-required" for="category_name">Tệp đính kèm</label>
                        <input accept=".pdf,.doc*" type="file" accept="file_extension" multiple class="form-control-file"
                            name="url" cols="30" rows="5" value="{{ old('url') }}">
                        @error('url')
                            <span class="mt-1 text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="category_name">Thumbnail</label>
                        <input type="file" accept="image/*" multiple class="form-control-file" name="thumbnail"
                            cols="30" rows="5">
                        @error('thumbnail')
                            <span class="mt-1 text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="type" class="label-required">Loại tài liệu</label>
                        <select name="type" value="{{ old('type') }}" id="type" class="form-control">
                            <option value=""></option>
                            @foreach (config('consts.documentLaw.type') as $type)
                                <option {{ old('type') == $type['value'] ? 'selected' : '' }} value="{{ $type['value'] }}">
                                    {{ $type['name'] }}</option>
                            @endforeach
                        </select>
                        @error('type')
                            <span class="mt-1 text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="category_name">Mô tả</label>
                        <textarea class="editor form-control" name="description" cols="30" rows="5">{{ old('description') }}</textarea>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Thêm mới</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('admin/product/add.js') }}"></script>
@endsection
