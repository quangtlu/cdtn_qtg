<div class="w3l_categories">
    <h4 class="side-bar-heading">Chuyên mục bài viết<i class="fa fa-times only-mobile close-list-category"></i></h4>
    <div class="side-bar-category">
        <ul role="tablist" aria-multiselectable="true">
            @foreach ($refrenceCategories as $index => $category)
                <li>
                    <a style="font-size: 18px; font-weight:bold" class="collapsed" role="button" data-toggle="collapse"
                        href="#collapse-{{ $category->id }}"
                        aria-expanded="{{ $category->categories->contains('id', request()->route('id')) ? 'true' : 'false' }}"
                        aria-controls="collapse-{{ $category->id }}">
                        {{ $index + 1 }}.{{ $category->name }}
                    </a>
                    @if ($category->categories->count())
                        <div id="collapse-{{ $category->id }}" class="panel-collapse {{ $category->categories->contains('id', request()->route('id')) ? 'in' : ''}} collapse" role="tabpanel"
                            aria-labelledby="headingTwo">
                            <ul style="padding-left: 15px">
                                @foreach ($category->categories as $indexChild => $categoryChild)
                                    <li id="caetegory-{{ $categoryChild->id }}">
                                        <a class="{{ request()->route('id') == $categoryChild->id ? 'active' : '' }}"
                                            href="{{ route('posts.getPostByCategory', ['id' => $categoryChild->id]) }}">
                                            <b>{{ $index + 1 . '.' . ($indexChild + 1) }}</b>.
                                            {{ $categoryChild->name }}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </li>
            @endforeach
        </ul>
    </div>
    <div class="side-bar-footer"></div>
</div>
