<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
        Thang Long University
    </div>
    <!-- Default to the left -->
    <strong>Copyright © 2022 Thang Long University. All rights reserved.</strong>
</footer>
