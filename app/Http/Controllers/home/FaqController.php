<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Faq;
use App\Services\FaqService;
use Illuminate\Http\Request;

use function view;

class FaqController extends Controller
{
    private $faqService;

    public function __construct(FaqService $faqService)
    {
        $this->faqService = $faqService;
    }

    public function index(Request $request)
    {
        try {
            $faqs = Faq::where('category_id', 'null')->latest()->get();
            $categories = Category::where('type', config('consts.category.type.faq.value'))->where('parent_id', 0)->get();
            if ($request->keyword) {
                $searchResults = $this->faqService->search($request);
                return view('home.faq.search-results', compact('searchResults'));
            }
            return view('home.faq.index', compact('faqs', 'categories'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.getData'));
        }
    }


    public function getByCategory($id)
    {
        try {
            $category = Category::find($id);
            $faqs = Faq::hasCategoryOrParentCategory($id)->paginate(10);
            return view('home.faq.list', compact('faqs', 'category'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.getData'));
        }
    }
}
