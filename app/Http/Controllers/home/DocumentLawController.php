<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\DocumentLaw;
use App\Services\DocumentLawService;
use Illuminate\Http\Request;

use function view;

class DocumentLawController extends Controller
{
    private $documentLawService;

    public function __construct(DocumentLawService $documentLawService)
    {
        $this->documentLawService = $documentLawService;
    }

    public function index(Request $request)
    {
        try {
            $query = DocumentLaw::query();
            $query->getByType(config('consts.documentLaw.type.other.value'));
            if ($request->keyword) {
                $query->search($request->keyword);
            }
            $documentLaws = $query->latest()->paginate(10);
            return view('home.documentLaws.other', compact('documentLaws'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.getData'));
        }
    }

    public function getPersonDocument(Request $request)
    {
        try {
            $query = DocumentLaw::query();
            $query->getByType(config('consts.documentLaw.type.person.value'));
            if ($request->keyword) {
                $query->search($request->keyword);
            }
            $documentLaws = $query->latest()->paginate(10);
            return view('home.documentLaws.person', compact('documentLaws'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.getData'));
        }
    }

    public function show($id)
    {
        try {
            $documentLaw = $this->documentLawService->getById($id);
            return view('home.documentLaws.show', compact('documentLaw'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }
}
