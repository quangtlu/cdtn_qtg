<?php

namespace App\Http\Controllers\Admin;

use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\Category\StoreCategoryRequest;
use App\Http\Controllers\Controller;
use App\Models\Category;

class CategoryController extends Controller
{
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
        $categoryReferences = $this->categoryService->getAll();
        view()->share([
            'categoryReferences' => $categoryReferences,
        ]);
    }

    public function index(Request $request)
    {
        try {
            if ($request->all()) {
                $query = Category::query();
                if ($request->keyword) {
                    $query->search($request->keyword);
                }
                if ($request->type) {
                    $query->type([$request->type]);
                }
                if ($request->category_id) {
                    $query->getCategoryOrParent($request->category_id);
                }
                $categories = $query->latest()->paginate(10);
            } else {
                $categories = $this->categoryService->getPaginate();
            }
            return view('admin.categories.index', compact('categories'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(StoreCategoryRequest $request)
    {
        try {
            $this->categoryService->create($request);
            return Redirect(route('admin.categories.index'))->with(
                'success',
                config('consts.message.success.create')
            );
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function edit($id)
    {
        try {
            $category = $this->categoryService->getById($id);
            return view('admin.categories.edit', compact('category'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function update(StoreCategoryRequest $request, $id)
    {
        try {
            $this->categoryService->update($request, $id);
            return Redirect(route('admin.categories.index'))->with(
                'success',
                config('consts.message.success.update')
            );
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function getType(Request $request)
    {
        $category_id = $request->category_id;
        if ($category_id) {
            $typeValue = $this->categoryService->getById($category_id)->type;
            $typeName = '';
            foreach (config('consts.category.type') as $type) {
                if ($type['value'] == $typeValue) {
                    $typeName = $type['name'];
                }
            }
            $htmlOptions = "<option value='$typeValue'>$typeName</option>";
        } else {
            $htmlOptions = '';
            foreach (config('consts.category.type') as $type) {
                $typeValue = $type['value'];
                $typeName = $type['name'];
                $htmlOptions .= "<option value='$typeValue'>$typeName</option>";
            }
        }
        return $htmlOptions;
    }

    public function destroy($id)
    {
        try {
            $category = $this->categoryService->delete($id);
            return response()->json([
                'category' => $category, 'message' => config('consts.message.success.delete')
            ]);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }
}
