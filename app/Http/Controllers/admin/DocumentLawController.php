<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\DocumentLaw\StoreDocumentLawRequest;
use App\Http\Requests\Admin\DocumentLaw\UpdateDocumentLawRequest;
use App\Services\DocumentLawService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DocumentLaw;

class DocumentLawController extends Controller
{
    private $documentLawService;

    public function __construct(DocumentLawService $documentLawService)
    {
        $this->documentLawService = $documentLawService;
    }

    public function index(Request $request)
    {
        try {
            $query = DocumentLaw::query();
            if ($request->keyword) {
                $query->search($query->keyword);
            }
            if ($request->type) {
                $query->getByType($request->type);
            }
            $documentLaws = $query->latest()->paginate(10);
            return view('admin.documentLaws.index', compact('documentLaws'));
        } catch (\Throwable $th) {
            dd($th->getMessage());
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function create()
    {
        return view('admin.documentLaws.create');
    }

    public function store(StoreDocumentLawRequest $request)
    {
        try {
            $this->documentLawService->create($request);
            return Redirect(route('admin.documentLaws.index'))->with(
                'success',
                config('consts.message.success.create')
            );
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function edit($id)
    {
        try {
            $documentLaw = $this->documentLawService->getById($id);
            $url = $documentLaw->url;
            $thumbnail = $documentLaw->thumbnail;
            return view('admin.documentLaws.edit', compact('documentLaw', 'url', 'thumbnail'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function update(UpdateDocumentLawRequest $request, $id)
    {
        try {
            $this->documentLawService->update($request, $id);
            return Redirect(route('admin.documentLaws.index'))->with(
                'success',
                config('consts.message.success.update')
            );
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function destroy($id)
    {
        try {
            $documentLaw = $this->documentLawService->delete($id);
            return response()->json([
                'documentLaw' => $documentLaw, 'message' => config('consts.message.success.delete')
            ]);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }
}
