<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Faq\StoreFaqRequest;
use App\Http\Requests\Admin\Faq\UpdateFaqRequest;
use App\Services\FaqService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Faq;

class FaqController extends Controller
{
    private $faqService;

    public function __construct(FaqService $faqService)
    {
        $this->faqService = $faqService;
        $categories = Category::where('type', config('consts.category.type.faq.value'))->get();
        view()->share('categories', $categories);
    }

    public function index(Request $request)
    {
        try {
            $query = Faq::query();
            if ($request->keyword) {
                $query->search($request->keyword);
            }
            if ($request->category_id) {
                $query->hasCategoryOrParentCategory($request->category_id);
            }
            $faqs = $query->latest()->paginate(10);
            return view('admin.faqs.index', compact('faqs'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function create()
    {
        return view('admin.faqs.create');
    }

    public function store(StoreFaqRequest $request)
    {
        try {
            $this->faqService->create($request);
            return Redirect(route('admin.faqs.index'))->with(
                'success',
                config('consts.message.success.create')
            );
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function edit($id)
    {
        try {
            $faq = $this->faqService->getById($id);
            return view('admin.faqs.edit', compact('faq'));
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function update(UpdateFaqRequest $request, $id)
    {
        try {
            $this->faqService->update($request, $id);
            return Redirect(route('admin.faqs.index'))->with(
                'success',
                config('consts.message.success.update')
            );
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }

    public function destroy($id)
    {
        try {
            $faq = $this->faqService->delete($id);
            return response()->json([
                'faq' => $faq, 'message' => config('consts.message.success.delete')
            ]);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', config('consts.message.error.common'));
        }
    }
}
