<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = ['question', 'answer', 'category_id'];

    public function scopeSearch($query, $keyword)
    {
        return $query->where('question', 'LIKE', "%{$keyword}%")
            ->orWhere('answer', 'LIKE', "%{$keyword}%")
            ->orWhere('id', 'LIKE', "%{$keyword}%");
    }

    public function scopeHasCategoryOrParentCategory($query, $categoryId)
    {
        if ($categoryId == 'null') {
            return $query->where('category_id', $categoryId);
        }
        return $query->whereHas('category', function ($subQuery) use ($categoryId) {
            $subQuery->where('id', $categoryId)->orWhere('parent_id', $categoryId);
        });
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
