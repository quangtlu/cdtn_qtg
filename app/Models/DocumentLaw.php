<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentLaw extends Model
{
    protected $table = 'document_law';
    protected $fillable = ['title', 'url', 'description', 'thumbnail', 'type'];

    public function scopeSearch($query, $keyword)
    {
        return $query->where('title', 'LIKE', "%{$keyword}%")
            ->orWhere('url', 'LIKE', "%{$keyword}%")->orWhere('description', 'LIKE', "%{$keyword}%");
    }

    public function scopeGetByType($query, $type)
    {
        return $query->where('type', $type);
    }
}
