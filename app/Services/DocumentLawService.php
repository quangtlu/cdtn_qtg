<?php

namespace App\Services;

use App\Models\DocumentLaw;
use App\traits\HandleImage;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;

class DocumentLawService
{
    private $documentLawModel;
    use HandleImage;

    public function __construct(DocumentLaw $documentLawModel)
    {
        $this->documentLawModel = $documentLawModel;
    }

    public function getPaginate()
    {
        $documentLaws = $this->documentLawModel->latest()->paginate(10);
        return $documentLaws;
    }

    public function search($request, $isAjax = false)
    {
        $documentLaws = $isAjax ? DocumentLaw::search($request->keyword)->get() : DocumentLaw::search($request->keyword)->paginate(10);
        return $documentLaws;
    }

    public function getAll()
    {
        $documentLaws = $this->documentLawModel->all();
        return $documentLaws;
    }

    public function getById($id)
    {
        $documentLaws = $this->documentLawModel->findOrFail($id);
        return $documentLaws;
    }

    public function create($request)
    {
        $file = $request->file('url');
        $time = Carbon::now('Asia/Ho_Chi_Minh')->format("Y.m.d H.i.s");
        $url = $time . '.' . $file->getClientOriginalName();
        $file->move('document', $url);
        $data = [
            "title" => $request->title,
            "description" => $request->description,
            "url" => $url,
            "type" => $request->type,
        ];
        if ($file = $request->file('thumbnail')) {
            $thumbnail = $time . '.' . $file->getClientOriginalName();
            $file->move('image/documentLaws', $thumbnail);
            $data["thumbnail"] = $thumbnail;
        }

        $this->documentLawModel->create($data);
    }

    public function update($request, $id)
    {
        $documentLaw = $this->getById($id);
        $data = [
            "title" => $request->title,
            "description" => $request->description,
            "type" => $request->type,
        ];

        $time = Carbon::now('Asia/Ho_Chi_Minh')->format("Y.m.d H.i.s");
        if ($file = $request->file('thumbnail')) {
            if (File::exists(public_path('image/documentLaws/' . $documentLaw->thumbnail))) {
                File::delete(public_path('image/documentLaws/' . $documentLaw->thumbnail));
            }
            $thumbnail = $time . '.' . $file->getClientOriginalName();
            $file->move('image/documentLaws', $thumbnail);
            $data['thumbnail'] = $thumbnail;
        }

        if ($file = $request->file('url')) {
            if (File::exists(public_path('document/' . $documentLaw->url))) {
                File::delete(public_path('document/' . $documentLaw->url));
            }
            $url = $time . '.' . $file->getClientOriginalName();
            $file->move('document', $url);
            $data['url'] = $url;
        }
        $documentLaw->update($data);
    }

    public function delete($id)
    {
        $documentLaw = $this->getById($id);
        if (File::exists(public_path('document/' . $documentLaw->url))) {
            File::delete(public_path('document/' . $documentLaw->url));
        }
        return $this->documentLawModel->destroy($id);
    }

    public function filter($request)
    {
        $documentLaws = DocumentLaw::query()->filterTitle($request)->paginate(10);
        return $documentLaws;
    }

    public function searchAndFilter($request)
    {
        $documentLaws = DocumentLaw::query()->filterTitle($request)->search($request->keyword)->paginate(10);
        return $documentLaws;
    }
}
